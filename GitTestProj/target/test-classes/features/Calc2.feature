Feature: Calculator 

#@Test-1
Scenario Outline: Calculate given numbers 
	Given Number <A> and number <B> 
	When Add A and B 
	Then Number C is equal to sum of A and B 
	
	Examples: 
		| A | B |
		| 1 | 5 |
		| 2 | 9 |
		| 2 | 4 |
		
#@Test-2 # tag to identify scenarios
Scenario Outline: Concatenate given Strings 
	Given String "<STR>" and number <N> 
	When Concatenate A and B 
	Then Concatenated value C is displayed in the output 
	
	Examples:  
		| STR   | N |
		| Hi    | 1 |
		| Hello | 2 |
		| Nice  | 3 |		
	
	