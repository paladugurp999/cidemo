package udemy.cukes.stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CalTestStepDefinitions {

	int A = 0;
	int B = 0;
	int C = 0;
	int N = 0;
	String str = null;

	@Given("^Number (\\d+) and number (\\d+)$")
	public void getNumbers(int A, int B) throws Throwable {
		this.A = A;
		this.B = B;
		System.out.println("Numbers: " + A + " and " + B);
	}

	@When("^Add A and B$")
	public void addNumbers() throws Throwable {
		C = A + B;
		System.out.println("Adding numbers...");
	}

	@Then("^Number C is equal to sum of A and B$")
	public void getResult() throws Throwable {

		System.out.println("Result: " + C);
	}

	@Given("^String \"([^\"]*)\" and number (\\d+)$")
	public void string_and_number(String arg1, int arg2) throws Throwable {
	str = arg1;
	N = arg2;
		System.out.println("String : "+arg1+" number: "+arg2);
	}

	@When("^Concatenate A and B$")
	public void concatenate_A_and_B() throws Throwable {
		System.out.println("Adding str and num...");
	}

	@Then("^Concatenated value C is displayed in the output$")
	public void concatenated_value_C_is_displayed_in_the_output() throws Throwable {
		System.out.println(str+": "+N);
//		Assert.assertEquals(actual, expected);

	}

}