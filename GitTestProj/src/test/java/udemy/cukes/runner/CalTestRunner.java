package udemy.cukes.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class) // for JUint
@CucumberOptions(dryRun = false, // default false, if it is true, its only checking step definitions without running the code
		monochrome = true, // output is more readable
		features="src/test/java/features/Calc.feature",
		name = { "Calculate given numbers","Concatenate given Strings" }, // {"@Test-1, @Test-2"} to run a particular scenario
		glue = "udemy.cukes.stepdefs") // checking step definitions file
//		plugin = { "pretty", "html:target/site/cucumber-pretty", "json:target/cucumber.json" }
		

public class CalTestRunner extends AbstractTestNGCucumberTests {
	

}
